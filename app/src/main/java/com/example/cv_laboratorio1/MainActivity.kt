package com.example.cv_laboratorio1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {

            if ( !edtEdad.text.isEmpty()) {

                val edad = edtEdad.text.toString().toInt()

                if (edad < 18) {
                    tvResultado.text = "Eres menor de edad!"
                } else {
                    tvResultado.text = "Eres mayor de edad!"
                }

            } else {
                tvResultado.text = ""
            }
        }

    }
}